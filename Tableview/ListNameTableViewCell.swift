//
//  ListNameTableViewCell.swift
//  Tableview
//
//  Created by Merin K Thomas on 04/01/23.
//

import UIKit

class ListNameTableViewCell: UITableViewCell {

    @IBOutlet weak var Photo: UIImageView!
    @IBOutlet weak var namelbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
