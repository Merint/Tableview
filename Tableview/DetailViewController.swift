//
//  DetailViewController.swift
//  Tableview
//
//  Created by Merin K Thomas on 04/01/23.
//

import UIKit

class DetailViewController: UIViewController {
    
    @IBOutlet weak var DetailName: UILabel!
    @IBOutlet weak var DtlPhoto: UIImageView!
    
    @IBOutlet weak var tableview: UITableView!
    

    
    var name = ""
    var image = UIImage()
    override func viewDidLoad() {
        super.viewDidLoad()
        DtlPhoto.image = image
        DetailName.text = "\(name)"
        
        // Do any additional setup after loading the view.
    }
}
    extension DetailViewController:UITableViewDelegate {
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            print("hai work")
        }
    }
extension DetailViewController:UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableview.dequeueReusableCell(withIdentifier: "seccell", for: indexPath) as! secTableViewCell
        cell.Dietlbl.text="Diet Challenge"
        return cell
    }
    
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */


