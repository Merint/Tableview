//
//  ViewController.swift
//  Tableview
//
//  Created by Merin K Thomas on 04/01/23.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var tableview: UITableView!

    var names = [
        "Jacob Cherian",
        "Annamma Cherian",
        "Joby K Jacob",
        "Merin K Thomas",
        
    ]
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        tableview.delegate = self
        tableview.dataSource = self
    }
}

    extension ViewController:UITableViewDelegate {
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//          print("hai work")
            let vc = storyboard?.instantiateViewController(withIdentifier: "DetailViewController")
            as?DetailViewController
            vc?.name = names[indexPath.row]
            vc?.image = UIImage(named: names[indexPath.row])!
            self.navigationController?.pushViewController(vc!, animated: true)
        }
        
        
    }
    
    extension ViewController:UITableViewDataSource {
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
             names.count
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//            let cell = tableview.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
//            cell.textLabel?.text = names[indexPath.row]
            let cell = tableview.dequeueReusableCell(withIdentifier: "cell") as! ListNameTableViewCell
            cell.namelbl.text = names[indexPath.row]
            cell.Photo.image = UIImage(named: names[indexPath.row])
            return cell
        }
        
        func tableView(_ tableView: UITableView,commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath)  {
            
            if editingStyle == UITableViewCell.EditingStyle.delete {
                names.remove(at: indexPath.row)
                tableview.reloadData()
            }
            
        }
    }
    
   



